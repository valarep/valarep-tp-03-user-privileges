﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valarep___TP_03___User_Privileges.Model.Dao;

namespace Valarep___TP_03___User_Privileges.Model.Classes
{
    public class User
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public List<Group> Groups { get; private set; }

        public User () { }

        public User (int id, string name, string userId, string password, string email)
        {
            Id = id;
            Name = name;
            UserId = userId;
            Password = password;
            Email = email;
        }

        public static User Get(string userId, string password)
        {
            return new UserDao().Get(userId, password);
        }
    }
}
