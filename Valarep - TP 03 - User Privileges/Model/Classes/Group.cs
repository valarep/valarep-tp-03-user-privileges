﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valarep___TP_03___User_Privileges.Model.Dao;

namespace Valarep___TP_03___User_Privileges.Model.Classes
{
    public class Group
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public List<User> Users { get; private set; }
        public List<Privilege> Privileges { get; private set; }

        public Group() { }

        public Group (int id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// Fournit la liste des groupes d'un utilisateur
        /// </summary>
        /// <param name="id">identifiant de l'utilisateur</param>
        /// <returns></returns>
        public static List<Group> GetAll(int id)
        {
            return new GroupDao().GetAll(id);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
