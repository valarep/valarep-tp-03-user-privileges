﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valarep___TP_03___User_Privileges.Model.Dao;

namespace Valarep___TP_03___User_Privileges.Model.Classes
{
    public class Privilege
    {
        public int? Id { get; set; }
        public string Name { get; set; }

        public List<Group> Groups { get; private set; }

        public Privilege() { }

        public Privilege(int id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// Fournit la liste des privilèges d'un groupe
        /// </summary>
        /// <param name="id">identifiant du groupe</param>
        /// <returns></returns>
        public static List<Privilege> GetAllByGroup(int id)
        {
            return new PrivilegeDao().GetAllByGroup(id);
        }

        /// <summary>
        /// Fournit la liste des privilèges d'un utilisateur
        /// </summary>
        /// <param name="id">identifiant de l'utilisateur</param>
        /// <returns></returns>
        public static List<Privilege> GetAllByUser(int id)
        {
            return new PrivilegeDao().GetAllByUser(id);
        }
    }
}
