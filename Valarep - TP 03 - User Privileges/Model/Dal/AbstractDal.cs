﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Valarep___TP_03___User_Privileges.Model.Dal
{
    abstract class AbstractDal
    {
        private MySqlConnectionStringBuilder connectionStringBuilder;
        protected MySqlConnection Connection { get; private set; }

        public AbstractDal()
        {
            connectionStringBuilder = new MySqlConnectionStringBuilder();
            connectionStringBuilder.Server = "127.0.0.1";
            connectionStringBuilder.Port = 3306;
            connectionStringBuilder.CharacterSet = "UTF8";
            connectionStringBuilder.Database = "user_privileges";
            connectionStringBuilder.UserID = "user_privileges";
            connectionStringBuilder.Password = "NxiEtCiAfpFRnTK5";
            connectionStringBuilder.PersistSecurityInfo = false;

            Connection = new MySqlConnection();
            Connection.ConnectionString = connectionStringBuilder.ConnectionString;
        }
    }
}
