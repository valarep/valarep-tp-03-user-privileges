﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valarep___TP_03___User_Privileges.Model.Dal;
using Valarep___TP_03___User_Privileges.Model.Classes;
using MySql.Data.MySqlClient;

namespace Valarep___TP_03___User_Privileges.Model.Dao
{
    class UserDao : AbstractDal
    {
        public User Get(string userId, string password)
        {
            User user = null;

            string cmdText = @"SELECT *
                               FROM  `user`
                               WHERE `user_id`  = ?userId
                               AND   `password` = ?password;";

            Connection.Open();
            MySqlCommand command = new MySqlCommand(cmdText, Connection);

            command.Prepare();
            command.Parameters.Add(new MySqlParameter("userId", userId));
            command.Parameters.Add(new MySqlParameter("password", password));

            MySqlDataReader reader = command.ExecuteReader();

            bool result = reader.Read();

            if (result)
            {
                int _id = (int)reader["id"];
                string _name = (string)reader["name"];
                string _userId = (string)reader["user_id"];
                string _password = (string)reader["password"];
                string _email = (string)reader["email"];

                user = new User(_id, _name, _userId, _password, _email);

            }
            reader.Close();
            Connection.Close();

            return user;
        }
    }
}
