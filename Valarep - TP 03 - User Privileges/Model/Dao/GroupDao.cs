﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valarep___TP_03___User_Privileges.Model.Dal;
using Valarep___TP_03___User_Privileges.Model.Classes;
using MySql.Data.MySqlClient;

namespace Valarep___TP_03___User_Privileges.Model.Dao
{
    class GroupDao : AbstractDal
    {
        /// <summary>
        /// Fournit tous les groupes d'un utilisateur
        /// </summary>
        /// <param name="id">identifiant de l'utilisateur</param>
        /// <returns></returns>
        public List<Group> GetAll (int id)
        {
            List<Group> groups = new List<Group>();

            string cmdText = @"
                SELECT *
                FROM  `group`
                INNER JOIN `user_group`
                    ON `user_group`.`id_group` = `group`.`id`
                WHERE `id_user` = ?id;
                ";

            Connection.Open();

            MySqlCommand command = Connection.CreateCommand();
            command.CommandText = cmdText;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", id));

            MySqlDataReader reader = command.ExecuteReader();

            while(reader.Read())
            {
                int _id = (int)reader["id"];
                string _name = (string)reader["name"];

                Group group = new Group(_id, _name);

                groups.Add(group);
            }

            reader.Close();
            Connection.Close();

            return groups;
        }
    }
}
