﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valarep___TP_03___User_Privileges.Model.Dal;
using Valarep___TP_03___User_Privileges.Model.Classes;
using MySql.Data.MySqlClient;

namespace Valarep___TP_03___User_Privileges.Model.Dao
{
    class PrivilegeDao : AbstractDal
    {
        /// <summary>
        /// Fournit tous les privilèges attribués à un groupe
        /// </summary>
        /// <param name="id">identifiant du groupe</param>
        /// <returns></returns>
        public List<Privilege> GetAllByGroup(int id)
        {
            List<Privilege> privileges = new List<Privilege>();

            string cmdText = @"
                SELECT `privilege`.*
                FROM  `privilege`
                INNER JOIN `group_privilege`
                    ON `group_privilege`.`id_privilege` = `privilege`.`id`
                WHERE `id_group` = ?id;
                ";

            Connection.Open();

            MySqlCommand command = Connection.CreateCommand();
            command.CommandText = cmdText;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", id));

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                int _id = (int)reader["id"];
                string _name = (string)reader["name"];

                Privilege privilege = new Privilege(_id, _name);

                privileges.Add(privilege);
            }

            reader.Close();
            Connection.Close();

            return privileges;
        }

        /// <summary>
        /// Fournit tous les privilèges attribués à un utilisateur
        /// </summary>
        /// <param name="id">identifiant de l'utilisateur</param>
        /// <returns></returns>
        public List<Privilege> GetAllByUser(int id)
        {
            List<Privilege> privileges = new List<Privilege>();

            string cmdText = @"
                SELECT DISTINCT `privilege`.*
                FROM  `privilege`
                INNER JOIN `group_privilege`
                    ON `group_privilege`.`id_privilege` = `privilege`.`id`
                INNER JOIN `user_group`
                    ON `user_group`.`id_group` = `group_privilege`.`id_group`
                WHERE `id_user` = ?id;
                ";

            Connection.Open();

            MySqlCommand command = Connection.CreateCommand();
            command.CommandText = cmdText;
            command.Prepare();
            command.Parameters.Add(new MySqlParameter("id", id));

            MySqlDataReader reader = command.ExecuteReader();

            while (reader.Read())
            {
                int _id = (int)reader["id"];
                string _name = (string)reader["name"];

                Privilege privilege = new Privilege(_id, _name);

                privileges.Add(privilege);
            }

            reader.Close();
            Connection.Close();

            return privileges;
        }
    }
}
