﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Valarep___TP_03___User_Privileges.Model.Classes;

namespace Valarep___TP_03___User_Privileges
{
    public partial class FrmAuthentication : Form
    {
        public User User { get; set; }

        public FrmAuthentication()
        {
            InitializeComponent();
        }

        private void btnConnect_Click(object sender, EventArgs e)
        {
            string userId = txtUserId.Text;
            string password = txtPassowrd.Text;
            User = User.Get(userId, password);
            if (User == null)
            {
                MessageBox.Show("User id or password error.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtPassowrd.Text = "";
                txtUserId.Focus();
                txtUserId.SelectAll();
            }
            else
            {
                // Controle du privilège FrmAuthentication::btnConnect
                List<Privilege> privileges = Privilege.GetAllByUser((int)User.Id);
                string privName = "FrmAuthentication::btnConnect";
                bool authorized = privileges.Find(p => p.Name == privName) != null;
                if (authorized)
                {
                    MessageBox.Show("Welcome " + User.Name + ".", "Welcome", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    DialogResult = DialogResult.OK;
                }
                else
                {
                    MessageBox.Show("Access refused. Please contact an administrator.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtPassowrd.Text = "";
                    txtUserId.Focus();
                    txtUserId.SelectAll();
                }
            }
        }
    }
}
