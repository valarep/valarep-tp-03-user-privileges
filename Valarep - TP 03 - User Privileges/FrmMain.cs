﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Valarep___TP_03___User_Privileges.Model.Classes;

namespace Valarep___TP_03___User_Privileges
{
    public partial class FrmMain : Form
    {
        public User User { get; set; }
        public List<Group> Groups { get; set; }
        public List<Privilege> Privileges { get; set; }

        public FrmMain()
        {
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            FrmAuthentication frm = new FrmAuthentication();
            DialogResult result = frm.ShowDialog();
            if (result == DialogResult.OK)
            {
                string privName;
                bool authorized;

                User = frm.User;
                lblUserId.Text = User.UserId;
                Groups = Group.GetAll((int)User.Id);
                lblGroups.Text = string.Join(",", Groups);
                Privileges = Privilege.GetAllByUser((int)User.Id);

                privName = "FrmMain::gestionDesPrivilègesToolStripMenuItem";
                authorized = Privileges.Find(p => p.Name == privName) != null;
                gestionDesPrivilègesToolStripMenuItem.Visible = authorized;

                privName = "FrmMain::gestionsDesGroupesToolStripMenuItem";
                authorized = Privileges.Find(p => p.Name == privName) != null;
                gestionsDesGroupesToolStripMenuItem.Visible = authorized;

                privName = "FrmMain::gestionsDesUtilisateursToolStripMenuItem";
                authorized = Privileges.Find(p => p.Name == privName) != null;
                gestionsDesUtilisateursToolStripMenuItem.Visible = authorized;
            }
            else
            {
                Application.Exit();
            }
        }

        private void quitterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void gestionDesPrivilègesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void gestionsDesGroupesToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void gestionsDesUtilisateursToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void déconnecterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            User = null;
            Groups = null;
            Privileges = null;
            lblUserId.Text = "";
            lblGroups.Text = "";

            Hide();
            FrmMain_Load(sender, e);
            Show();
        }
    }
}
