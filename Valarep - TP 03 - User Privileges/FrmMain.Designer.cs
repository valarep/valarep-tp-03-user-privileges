﻿namespace Valarep___TP_03___User_Privileges
{
    partial class FrmMain
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblUserId = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblGroups = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fichierToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.quitterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.outilsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesPrivilègesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionsDesGroupesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionsDesUtilisateursToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.déconnecterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblUserId,
            this.lblGroups});
            this.statusStrip1.Location = new System.Drawing.Point(0, 428);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(800, 22);
            this.statusStrip1.TabIndex = 0;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // lblUserId
            // 
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(53, 17);
            this.lblUserId.Text = "lblUserId";
            // 
            // lblGroups
            // 
            this.lblGroups.Name = "lblGroups";
            this.lblGroups.Size = new System.Drawing.Size(58, 17);
            this.lblGroups.Text = "lblGroups";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fichierToolStripMenuItem,
            this.outilsToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fichierToolStripMenuItem
            // 
            this.fichierToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.déconnecterToolStripMenuItem,
            this.quitterToolStripMenuItem});
            this.fichierToolStripMenuItem.Name = "fichierToolStripMenuItem";
            this.fichierToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.fichierToolStripMenuItem.Text = "Fichier";
            // 
            // quitterToolStripMenuItem
            // 
            this.quitterToolStripMenuItem.Name = "quitterToolStripMenuItem";
            this.quitterToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.quitterToolStripMenuItem.Text = "Quitter";
            this.quitterToolStripMenuItem.Click += new System.EventHandler(this.quitterToolStripMenuItem_Click);
            // 
            // outilsToolStripMenuItem
            // 
            this.outilsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionDesPrivilègesToolStripMenuItem,
            this.gestionsDesGroupesToolStripMenuItem,
            this.gestionsDesUtilisateursToolStripMenuItem});
            this.outilsToolStripMenuItem.Name = "outilsToolStripMenuItem";
            this.outilsToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.outilsToolStripMenuItem.Text = "Outils";
            // 
            // gestionDesPrivilègesToolStripMenuItem
            // 
            this.gestionDesPrivilègesToolStripMenuItem.Name = "gestionDesPrivilègesToolStripMenuItem";
            this.gestionDesPrivilègesToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.gestionDesPrivilègesToolStripMenuItem.Text = "Gestion des Privilèges";
            this.gestionDesPrivilègesToolStripMenuItem.Click += new System.EventHandler(this.gestionDesPrivilègesToolStripMenuItem_Click);
            // 
            // gestionsDesGroupesToolStripMenuItem
            // 
            this.gestionsDesGroupesToolStripMenuItem.Name = "gestionsDesGroupesToolStripMenuItem";
            this.gestionsDesGroupesToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.gestionsDesGroupesToolStripMenuItem.Text = "Gestions des Groupes";
            this.gestionsDesGroupesToolStripMenuItem.Click += new System.EventHandler(this.gestionsDesGroupesToolStripMenuItem_Click);
            // 
            // gestionsDesUtilisateursToolStripMenuItem
            // 
            this.gestionsDesUtilisateursToolStripMenuItem.Name = "gestionsDesUtilisateursToolStripMenuItem";
            this.gestionsDesUtilisateursToolStripMenuItem.Size = new System.Drawing.Size(201, 22);
            this.gestionsDesUtilisateursToolStripMenuItem.Text = "Gestions des Utilisateurs";
            this.gestionsDesUtilisateursToolStripMenuItem.Click += new System.EventHandler(this.gestionsDesUtilisateursToolStripMenuItem_Click);
            // 
            // déconnecterToolStripMenuItem
            // 
            this.déconnecterToolStripMenuItem.Name = "déconnecterToolStripMenuItem";
            this.déconnecterToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.déconnecterToolStripMenuItem.Text = "Déconnecter";
            this.déconnecterToolStripMenuItem.Click += new System.EventHandler(this.déconnecterToolStripMenuItem_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.Text = "FrmMain";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblUserId;
        private System.Windows.Forms.ToolStripStatusLabel lblGroups;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fichierToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem quitterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem outilsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesPrivilègesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionsDesGroupesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionsDesUtilisateursToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem déconnecterToolStripMenuItem;
    }
}

